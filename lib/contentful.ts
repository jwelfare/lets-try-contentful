import { createClient } from "contentful";

const CONTENTFUL_DELIVERY_API_HOST = "cdn.contentful.com";
const CONTENTFUL_PREVIEW_API_HOST = "preview.contentful.com"

export const getClient = (preview: boolean) => {  
  const space = process.env.CONTENTFUL_SPACE_ID;
  const environment = process.env.CONTENTFUL_ENVIRONMENT;

  const accessToken = preview ? process.env.CONTENTFUL_PREVIEW_ACCESS_TOKEN : process.env.CONTENTFUL_ACCESS_TOKEN;
  const host = preview ? CONTENTFUL_PREVIEW_API_HOST : CONTENTFUL_DELIVERY_API_HOST;

  if (!space) throw Error("Missing required CONTENT_SPACE_ID environment variable");
  if (!environment) throw new Error("Missing required CONTENTFUL_ENVIRONMENT environment variable")
  if (!accessToken) throw new Error("Missing required CONTENTFUL_PREVIEW_ACCESS_TOKEN or CONTENTFUL_ACCESS_TOKEN environment variable")

  return createClient({
    space, 
    accessToken, 
    environment, 
    host
  })
}

