import { NextApiHandler } from 'next'

const Preview: NextApiHandler = async (req, res) => {
  res.clearPreviewData()

  // redirect the user back to the homepage with preview mode enabled
  // todo: dynamic redirect 
  res.setHeader('Content-Type', 'text/html');
  res.write(
    `<!DOCTYPE html><html><head><meta http-equiv="Refresh" content="0; url=/" />
    <script>window.location.href = '/'</script>
    </head>
    </html>`
  );

  res.end();
}

export default Preview;