// This handler is requested by Contentful when previewing draft content
// It sets up the preview cookies on the users' browser and allows for
// pages to fetch alternative data based on a context value (ctx.previewMode)

import { NextApiHandler } from 'next'

const Preview: NextApiHandler = async (req, res) => {
  const { secret, slug } = req.query;

  if (secret !== process.env.CONTENTFUL_PREVIEW_SECRET || !slug) {
    return res.status(401).json({ message: "Invalid token" })
  }

  res.setPreviewData({});

  res.redirect(slug as string);
}

export default Preview;