import Head from 'next/head'
import { Entry } from 'contentful'
import { getClient } from '../lib/contentful';
import Link from 'next/link';

interface IHomeProps {
  entries: Entry<unknown>[],
  preview: boolean,
}

const Home = ({ entries, preview }: IHomeProps) => {
  const stringifiedEntries = JSON.stringify(entries, null, 2);

  return (
    <>
      <Head>
        <title>{`${preview ? '[PREVIEW]: ' : ''}Lets try Contentful`}</title>
      </Head>

      {preview && (
        <>
          You are using preview mode, <Link prefetch={false} href="/api/exit-preview">click here to exit!</Link>
        </>
      )}

      <h1>All contentful entries: </h1>
      <pre>
        {stringifiedEntries}
      </pre>
    </>
  )
}

export const getStaticProps = async ({ preview = false }) => {
  const client = getClient(preview);

  const entries = await client.getEntries();

  return {
    props: {
      preview,
      entries
    }
  }
}

export default Home
