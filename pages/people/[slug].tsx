import { GetStaticProps, InferGetStaticPropsType } from "next";
import Head from "next/head";
import Link from "next/link";
import { getClient } from "../../lib/contentful";

const Person = ({ entry, preview }: InferGetStaticPropsType<typeof getStaticProps>) => {
  const stringifiedEntry = JSON.stringify(entry, null, 2);

  return (
    <>
      <Head>
        <title>{`${preview ? '[PREVIEW]: ' : ''}Lets try Contentful`}</title>
      </Head>

      {preview && (
        <>
          You are using preview mode, <Link prefetch={false} href="/api/exit-preview">click here to exit!</Link>
        </>
      )}

      <pre>
        {stringifiedEntry}
      </pre>
    </>
  )
}

export const getStaticProps: GetStaticProps = async ({ params, preview = false }) => {
  const client = getClient(preview);

  const entry = await client.getEntries<any>({ content_type: 'person', 'fields.slug': params?.slug, limit: 1 });

  return {
    props: {
      preview,
      entry: entry.items[0]
    }
  }
}

export const getStaticPaths = async ({ preview = false }) => {
  const client = getClient(preview);

  // todo: not sure if you'd type this?
  const entries = await client.getEntries<any>({ content_type: 'person' })

  const paths = entries.items.map((entry) => ({
    params: {
      slug: entry.fields.slug,
    }
  }));

  return {
    paths,
    fallback: false
  }
}

export default Person;